package com.example.springboottest.utils;

import com.example.springboottest.common.exception.UserException;
import com.example.springboottest.common.out.ResultDto;

/**
 * @author Abel
 * Response message processing
 */
public class ResUtil {

    /**
     * Communication successful
     *
     * @return
     */
    public static ResultDto success() {
        return success(null);
    }

    /**
     * Communication successful
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResultDto<T> success(T data) {
        ResultDto dto = ResultDto.getInstance();
        dto.setSuccess(Boolean.TRUE);
        dto.setCode("OK");
        dto.setMessage("OK");
        dto.setData(data);
        return dto;
    }

    /**
     * Communication failure
     *
     * @param userExceptionType
     * @return
     */
    public static ResultDto fail(UserException.UserExceptionType userExceptionType) {
        return fail(userExceptionType.getCode(), userExceptionType.getDesc());
    }

    /**
     * Communication failure
     *
     * @param code
     * @param message
     * @return
     */
    public static ResultDto fail(String code, String message) {
        ResultDto dto = ResultDto.getInstance();
        dto.setSuccess(Boolean.FALSE);
        dto.setCode(code);
        dto.setMessage(message);
        return dto;
    }

    /**
     * @return
     */
    public static ResultDto fail() {
        return fail("fail", "失败");
    }
}
