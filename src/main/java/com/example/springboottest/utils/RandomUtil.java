package com.example.springboottest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomUtil {
    private final static Logger logger = LoggerFactory.getLogger(RandomUtil.class);


    public static int getRandomInt() {
        Random random = new Random();
        return random.nextInt();
    }
    public static int getRandomIntBySecureRandom() {
        Random random = new SecureRandom();
        return random.nextInt();
    }

    public static void main(String[] args) {
        logger.info(""+getRandomInt());
        logger.info(""+getRandomIntBySecureRandom());
        asy();
    }

    public static void asy(){
        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    logger.info(Thread.currentThread().getName()+":"+ThreadLocalRandom.current().nextInt());
                }
            });
            thread.setName("thread"+i);
            thread.start();
        }
    }

}
