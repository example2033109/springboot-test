package com.example.springboottest.service.impl;

import com.example.springboottest.entity.UserInfo;
import com.example.springboottest.mappers.UserInfoMapper;
import com.example.springboottest.service.UserService;
import com.example.springboottest.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    //    @Autowired // @Autowired 默认按类型注入
    @Resource
    UserInfoMapper userInfoMapper;

    public Boolean createUser(UserVo vo) {
        UserInfo user = new UserInfo();
        user.setName(vo.getName());
        user.setPwd(vo.getPwd());
        user.setRole(vo.getRole());
        user.setAccount(vo.getAccount());
        user.setPhone(vo.getPhone());
        user.setEmail(vo.getEmail());
        user.setNickname(vo.getNickname());
        if (userInfoMapper.insert(user) == 1) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public List<UserInfo> selectByAccount(String account) {
        return userInfoMapper.selectByAccount(account);
    }
    @Override
    public List<UserInfo> selectByName(String name) {
        return userInfoMapper.selectByName(name);
    }
}
