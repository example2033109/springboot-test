package com.example.springboottest.service;

import com.example.springboottest.entity.UserInfo;
import com.example.springboottest.vo.UserVo;

import java.util.List;

public interface UserService {

    /**
     * @param vo
     * @return
     */
    Boolean createUser(UserVo vo);

    /**
     * @param name
     * @return
     */
    List<UserInfo> selectByName(String name);

    List<UserInfo> selectByAccount(String account);
}
