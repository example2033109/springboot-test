package com.example.springboottest.common.exception;

public class UserException extends TradeException {

    public UserException(String code, String message) {
        super(message);
        super.setCode(code);
        super.setMessage(message);
    }

    public UserException(UserExceptionType userExceptionType) {
        super(userExceptionType.getDesc());
        super.setCode(userExceptionType.getCode());
        super.setMessage(userExceptionType.getDesc());
    }

    /**
     * 0-2 应用
     * 3-6 业务模块
     * 6-10 序号
     */
    public enum UserExceptionType {
        YUAU0000001("YUAU0000001", "账号已经被使用"),
        YUAU0000002("YUAU0000002", "用户不存在"),
        YUAU0000003("YUAU0000003", "用户名不能为空"),
        YUAU0000004("YUAU0000004", "用户号仅可以由6-20位的英文字母，数字，_组成"),
        YUAU0000005("YUAU0000005", "手机号码不合法"),
        YUAU0000006("YUAU0000006", "请输入昵称"),
        YUAU0000007("YUAU0000007", "请输入密码"),
        YUAU0000999("YUAU0000999", "查询客户信息失败");

        UserExceptionType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        private String code;
        private String desc;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }
}
