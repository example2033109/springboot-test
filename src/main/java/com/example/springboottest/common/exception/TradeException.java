package com.example.springboottest.common.exception;

import io.swagger.annotations.ApiModelProperty;

public class TradeException extends RuntimeException {
    private static long serialVersionUID = 1l;
    private String code;
    private String message;

    public TradeException() {
    }

    public TradeException(String message) {
        super(message);
    }

    public TradeException(String message, Throwable cause) {
        super(message, cause);
    }

    public TradeException(Throwable cause) {
        super(cause);
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
