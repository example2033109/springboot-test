package com.example.springboottest.common.interceptor;

import com.example.springboottest.common.exception.TradeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ControllerExceptionInterceptor implements HandlerExceptionResolver {
    private static Logger logger = LoggerFactory.getLogger(ControllerExceptionInterceptor.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        if (ex instanceof TradeException) {
            mv.addObject("code", ((TradeException) ex).getCode());
            mv.addObject("message", ex.getMessage());
        } else {
            mv.addObject("code", "fail");
            mv.addObject("message", ex.getMessage());
        }
        logger.error("请求{}发生异常！", request.getRequestURI());
        logger.error("异常信息栈： ", ex);
        return mv;
        // 需要返回空的ModelAndView以阻止异常继续被其它处理器捕获
//        return mv;
// 返回null将不会拦截异常，其它处理器可以继续处理该异常
//        return null;
    }
}
