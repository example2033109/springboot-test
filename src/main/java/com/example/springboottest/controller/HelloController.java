package com.example.springboottest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {


    @RequestMapping("/")
    @ResponseBody
    public String sayHello() {
        return "Hello, SpringBoot With Docker";
    }

    @RequestMapping("/test/test")
    @ResponseBody
    public String haha() {
        return "Hello, SpringBoot With Docker";
    }
    @RequestMapping("/test/test2")
    @ResponseBody
    public String haha2() {
        return "Hello, SpringBoot With Docker";
    }

}
