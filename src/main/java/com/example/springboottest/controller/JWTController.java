package com.example.springboottest.controller;

import com.example.springboottest.common.exception.UserException;
import com.example.springboottest.common.out.ResultDto;
import com.example.springboottest.entity.UserInfo;
import com.example.springboottest.service.UserService;
import com.example.springboottest.utils.JwtUtils;
import com.example.springboottest.utils.ResUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class JWTController {
    private static Logger logger = LoggerFactory.getLogger(JWTController.class);

    @Autowired
    UserService userService;

    @PostMapping("/jwt/query")
    @ResponseBody
    public ResultDto<String> getJWT(@RequestBody String account) {
        try {
            UserInfo userInfo = userService.selectByAccount(account).get(0);
            String jwt = JwtUtils.getJwtToken(userInfo.getAccount(), userInfo.getNickname());
            return ResUtil.success(jwt);
        } catch (Exception e) {
            logger.error("获取token", e);
            throw new UserException(UserException.UserExceptionType.YUAU0000002);
        }
    }

    @PostMapping("/jwt/handle")
    @ResponseBody
    public ResultDto<String> verifyToken(@RequestBody String token) {
        try {
            return ResUtil.success(JwtUtils.verifyToken(token));
        } catch (Exception e) {
            logger.error("获取token", e);
            throw new UserException(UserException.UserExceptionType.YUAU0000002);
        }
    }
}
