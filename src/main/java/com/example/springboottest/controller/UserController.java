package com.example.springboottest.controller;

import com.example.springboottest.common.exception.UserException;
import com.example.springboottest.dto.UserDto;
import com.example.springboottest.entity.UserInfo;
import com.example.springboottest.service.UserService;
import com.example.springboottest.common.out.ResultDto;
import com.example.springboottest.utils.ResUtil;
import com.example.springboottest.vo.UserVo;
import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.regex.Pattern;

@Controller
public class UserController {
    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;


    /**
     * Create user
     *
     * @param vo
     * @return
     */
    @PostMapping("/c/createUser")
    @ResponseBody
    public ResultDto createUser(@RequestBody UserVo vo) {
        //校验
        checkBeforereateUser(vo);
        try {
            if (userService.createUser(vo)) {
                return ResUtil.success();
            } else {
                return ResUtil.fail(UserException.UserExceptionType.YUAU0000001);
            }
        } catch (Exception e) {
            logger.error("创建客户：", e);
            return ResUtil.fail(UserException.UserExceptionType.YUAU0000001);
        }
    }

    private void checkBeforereateUser(UserVo vo) {
        // 用户名
        if (vo.getName() == null || vo.getName().isEmpty()) {
            throw new UserException(UserException.UserExceptionType.YUAU0000003);
        }
        // 账号
        if (vo.getAccount().length() < 6 || vo.getAccount().length() > 20 || Pattern.compile("[^A-Za-z0-9_]").matcher(vo.getAccount()).find()) {
            throw new UserException(UserException.UserExceptionType.YUAU0000004);
        }
        // 密码
        if (vo.getPwd() == null || vo.getPwd().isEmpty()) {
            throw new UserException(UserException.UserExceptionType.YUAU0000007);
        }
        // 手机号
        if (!Pattern.compile("^[1]\\d{10}$").matcher(vo.getPhone()).matches()) {
            throw new UserException(UserException.UserExceptionType.YUAU0000005);
        }
        // 昵称
        if (vo.getNickname() == null || vo.getNickname().length() < 1) {
            throw new UserException(UserException.UserExceptionType.YUAU0000006);
        }
    }


    @PostMapping("/c/query/user")
    @ResponseBody
    public ResultDto<UserDto> queryUserInfo(@RequestBody String name) {
        List<UserInfo> userInfo = userService.selectByName(name);
        if (userInfo == null || userInfo.size() < 1) {
            return ResUtil.fail(UserException.UserExceptionType.YUAU0000002);
        } else {
            return ResUtil.success(UserDto.from(userInfo.get(0)));
        }
    }
}
