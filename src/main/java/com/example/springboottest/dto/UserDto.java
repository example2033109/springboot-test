package com.example.springboottest.dto;

import com.example.springboottest.entity.UserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户信息
 *
 * @author Abel
 */
@ApiModel("用户信息")
public class UserDto {

    @ApiModelProperty("用户名")
    private String name;
    @ApiModelProperty("角色")
    private Integer role;
    @ApiModelProperty("手机号码")
    private String phone;
    @ApiModelProperty("电子邮箱")
    private String email;
    @ApiModelProperty("账号")
    String account;
    @ApiModelProperty("昵称")
    private String nickname;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public static UserDto from(UserInfo userInfo) {
        UserDto dto = new UserDto();
        dto.setAccount(userInfo.getAccount());
        dto.setName(userInfo.getName());
        dto.setNickname(userInfo.getNickname());
        dto.setPhone(userInfo.getPhone());
        dto.setRole(userInfo.getRole());
        dto.setEmail(userInfo.getEmail());

        return dto;
    }
}
